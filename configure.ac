dnl configure.ac -- autoconf script for the m17n-im-config package.

dnl Copyright (C) 2007
dnl   National Institute of Advanced Industrial Science and Technology (AIST)
dnl   Registration Number H15PRO112

dnl This file is part of the m17n-im-config package; a sub-part of the
dnl m17n library..

dnl The m17n library is free software; you can redistribute it and/or
dnl modify it under the terms of the GNU Lesser General Public License
dnl as published by the Free Software Foundation; either version 2.1 of
dnl the License, or (at your option) any later version.

dnl The m17n library is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl Lesser General Public License for more details.

dnl You should have received a copy of the GNU Lesser General Public
dnl License along with the m17n library; if not, write to the Free
dnl Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
dnl Boston, MA 02110-1301, USA.

dnl Process this file with autoconf to produce a configure script.

AC_INIT(m17n-im-config, 0.9.0, m17n-lib-bug@m17n.org)
AM_INIT_AUTOMAKE
AM_CONFIG_HEADER(src/config.h)

AC_GNU_SOURCE
AM_GNU_GETTEXT([external], [need-ngettext])
AM_GNU_GETTEXT_VERSION(0.14.4)
GETTEXTDIR="$datadir/locale"
AC_SUBST(GETTEXTDIR)

dnl Checks for programs for compiling.
AC_PROG_CC
AM_PROG_LIBTOOL

dnl Checks for standard header files.
AC_HEADER_STDC

dnl Checks for the M17N library.
PKG_CHECK_MODULES(M17NLIB, [m17n-shell >= 1.3.4])
PKG_CHECK_MODULES(M17NDB, [m17n-db >= 1.3.4])
AC_SUBST(M17NLIB_CFLAGS)
AC_SUBST(M17NLIB_LDFLAGS)

dnl Check for Gtk+ library.
PKG_CHECK_MODULES(GTK2, [gtk+-2.0 >= 2.4.0])

AC_ARG_ENABLE(program,
	      AC_HELP_STRING([--enable-program],
			     [enable a standalone program m17n-im-config (default is YES)]))
AM_CONDITIONAL(ENABLE_PROG, test x$enable_program != xno)

AC_CONFIG_FILES([Makefile po/Makefile.in po/Makefile
		 m17n-im-config.pc			  
                 src/Makefile
		 ])

AC_OUTPUT

dnl Local Variables:
dnl comment-start: "dnl "
dnl comment-end: ""
dnl comment-start-skip: "\\bdnl\\b\\s *"
dnl End:
