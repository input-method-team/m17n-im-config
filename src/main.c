/* main.c -- M17N input method configuration tool
   Copyright (C) 2007
     National Institute of Advanced Industrial Science and Technology (AIST)
     Registration Number H15PRO112

   This file is part of the m17n-im-config package; a sub-part of the
   m17n library.

   The m17n library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 2.1 of
   the License, or (at your option) any later version.

   The m17n library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the m17n library; if not, write to the Free
   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   02111-1307, USA.  */

#include <stdlib.h>
#include <string.h>
#include <libintl.h>
#define _(String) gettext (String)
#include <gtk/gtk.h>
#include <m17n-im-config.h>

/* Common argument to all callback functions.  */

struct CallbackArgument
{
  /* IM configuration widget created by mim_config_new ().  */
  GtkWidget *config;
  /* Button widget "default" */
  GtkWidget *default_;
  /* Button widget "revert" */
  GtkWidget *revert;
  /* Button widget "save" */
  GtkWidget *save;
};


/* Called when the status of configuration is changed.  */
void
status_changed_cb (GtkWidget *config, gpointer data)
{
  struct CallbackArgument *arg = data;
  gboolean modified = mim_config_modified (config);

  if (GTK_WIDGET_SENSITIVE (arg->save))
    {
      if (! modified)
	{
	  gtk_widget_set_sensitive (arg->revert, FALSE);
	  gtk_widget_set_sensitive (arg->save, FALSE);
	}
    }
 else
   {
      if (modified)
	{
	  gtk_widget_set_sensitive (arg->revert, TRUE);
	  gtk_widget_set_sensitive (arg->save, TRUE);
	}
   }
}

/* Called when "default" button is clicked.  */
static void
default_clicked_cb (GtkButton *button, gpointer data)
{
  struct CallbackArgument *arg = data;

  mim_config_default (arg->config);
  status_changed_cb (arg->config, data);
}

/* Called when "revert" button is clicked.  */
static void
revert_clicked_cb (GtkButton *button, gpointer data)
{
  struct CallbackArgument *arg = data;

  mim_config_revert (arg->config);
  status_changed_cb (arg->config, data);
}

/* Called when "save" button is clicked.  */
static void
save_clicked_cb (GtkButton *button, gpointer data)
{
  struct CallbackArgument *arg = data;

  mim_config_save (arg->config);
  status_changed_cb (arg->config, data);
}

/* Called when "quit" button is clicked.  */
static void
quit_clicked_cb (GtkButton *button, gpointer data)
{
  struct CallbackArgument *arg = data;

  if (mim_config_modified (arg->config))
    {
      GtkWidget *dialog, *label;
      gint response;

      dialog = (gtk_dialog_new_with_buttons
		("confirmation",
		 GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (button))),
		 GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_NO_SEPARATOR,
		 GTK_STOCK_NO, GTK_RESPONSE_NO,
		 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		 GTK_STOCK_SAVE, GTK_RESPONSE_YES,
		 NULL));
      label = gtk_label_new (_("Save configuration?"));
      gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), label);
      gtk_widget_show_all (dialog);
      response = gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);
      if (response == GTK_RESPONSE_CANCEL)
	return;
      if (response == GTK_RESPONSE_YES)
	mim_config_save (arg->config);
    }
  gtk_widget_destroy (arg->config);
  gtk_main_quit ();      
}

/* Called when "ok" button is clicked.  */
static void
ok_clicked_cb (GtkButton *button, gpointer data)
{
  struct CallbackArgument *arg = data;

  if (mim_config_modified (arg->config))
    mim_config_save (arg->config);
  gtk_widget_destroy (arg->config);
  gtk_main_quit ();      
}

int
main (int argc, char **argv)
{
  GtkWidget *window, *vbox, *hbox;
  GtkWidget *default_, *revert, *save, *ok, *quit;
  struct CallbackArgument arg;
    
#if ENABLE_NLS
  bindtextdomain ("m17n-im-config", GETTEXTDIR);
  bind_textdomain_codeset ("m17n-im-config", "UTF-8");
#endif
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_size_request (window, 500, 300);
  gtk_window_set_title (GTK_WINDOW (window), _("M17N-IM Configuration"));
  g_signal_connect (G_OBJECT (window), "destroy",
		    G_CALLBACK (gtk_main_quit), NULL);
    
  /* We create these widgets:
     +-vbox----------------------------------+
     |+-config------------------------------+|
     ||                                     ||
     ||                                     ||
     ||  M17N-IM Configuration              ||
     ||                                     ||
     ||                                     ||
     |+-------------------------------------+|
     |+-hbox--------------------------------+|
     ||+-------+ +------+ +----+ +----+ +--+||
     |||default| |revert| |save| |quit| |ok|||
     ||+-------+ +------+ +----+ +----+ +--+||
     |+-------------------------------------+|
     +---------------------------------------+
  */

  vbox = gtk_vbox_new (FALSE, 12);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  default_ = gtk_button_new_from_stock (_("_Default"));
  revert = gtk_button_new_from_stock (GTK_STOCK_REVERT_TO_SAVED);
  gtk_widget_set_sensitive (revert, FALSE);
  save = gtk_button_new_from_stock (GTK_STOCK_SAVE);
  gtk_widget_set_sensitive (save, FALSE);
  quit = gtk_button_new_from_stock (GTK_STOCK_QUIT);
  ok = gtk_button_new_from_stock (GTK_STOCK_OK);

  arg.revert = revert;
  arg.save = save;
  arg.config = mim_config_new (G_CALLBACK (status_changed_cb), &arg);
  gtk_box_pack_start (GTK_BOX (vbox), arg.config, TRUE, TRUE, 0);

  g_signal_connect (G_OBJECT (default_), "clicked",
		    G_CALLBACK (default_clicked_cb), &arg);
  g_signal_connect (G_OBJECT (revert), "clicked",
		    G_CALLBACK (revert_clicked_cb), &arg);
  g_signal_connect (G_OBJECT (save), "clicked",
		    G_CALLBACK (save_clicked_cb), &arg);
  g_signal_connect (G_OBJECT (quit), "clicked",
		    G_CALLBACK (quit_clicked_cb), &arg);
  g_signal_connect (G_OBJECT (ok), "clicked",
		    G_CALLBACK (ok_clicked_cb), &arg);

  hbox = gtk_hbutton_box_new ();
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbox), GTK_BUTTONBOX_END);
  gtk_box_set_spacing (GTK_BOX (hbox), 5);
  gtk_container_add (GTK_CONTAINER (hbox), default_);
  gtk_container_add (GTK_CONTAINER (hbox), revert);
  gtk_container_add (GTK_CONTAINER (hbox), save);
  gtk_container_add (GTK_CONTAINER (hbox), quit);
  gtk_container_add (GTK_CONTAINER (hbox), ok);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 5);

  gtk_widget_show_all (window);
  gtk_main ();

  return 0;
}
