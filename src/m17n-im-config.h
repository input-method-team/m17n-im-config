/* m17n-im-config.c -- M17N input method configuration header file
   Copyright (C) 2007
     National Institute of Advanced Industrial Science and Technology (AIST)
     Registration Number H15PRO112

   This file is part of the m17n-im-config package; a sub-part of the
   m17n library.

   The m17n library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation; either version 2.1 of
   the License, or (at your option) any later version.

   The m17n library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the m17n library; if not, write to the Free
   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   02111-1307, USA.  */

#ifndef _M17N_IM_CONFIG_H_
#define _M17N_IM_CONFIG_H_

#include <glib.h>

G_BEGIN_DECLS

extern GtkWidget *mim_config_new (GCallback func, gpointer data);
extern gboolean mim_config_modified (GtkWidget *config);
extern gboolean mim_config_revert (GtkWidget *config);
extern gboolean mim_config_default (GtkWidget *config);
extern gboolean mim_config_save (GtkWidget *config);
extern GtkTreeView *mim_config_get_tree_view (GtkWidget *config);

G_END_DECLS

#endif /* _M17N_IM_CONFIG_H_ */
