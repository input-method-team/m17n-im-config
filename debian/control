Source: m17n-im-config
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 Boyuan Yang <byang@debian.org>,
Build-Depends:
 debhelper (>= 11),
 libgtk2.0-dev,
 libm17n-dev,
 m17n-db,
 pkg-config (>= 0.22),
Standards-Version: 4.2.1
Homepage: https://www.m17n.org/
Vcs-Git: https://salsa.debian.org/input-method-team/m17n-im-config.git
Vcs-Browser: https://salsa.debian.org/input-method-team/m17n-im-config

Package: libm17n-im-config-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libm17n-dev,
 libm17n-im-config0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: input method configuration library for m17n-lib - development
 m17n-im-config is a library to create a GTK+ widget for per-user
 configuration of input methods provided by the m17n library, and a
 standalone GUI program which demonstrates the library.  m17n is an
 abbreviation of Multilingualization.
 .
 This package contains the header and development files needed to build
 programs and packages using the m17n-im-config library.

Package: libm17n-im-config0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 libm17n-0,
 ${misc:Depends},
 ${shlibs:Depends},
Description: input method configuration library for m17n-lib - runtime
 m17n-im-config is a library to create a GTK+ widget for per-user
 configuration of input methods provided by the m17n library, and a
 standalone GUI program which demonstrates the library.  m17n is an
 abbreviation of Multilingualization.
 .
 This package contains the runtime part of the m17n-im-config library.

Package: m17n-im-config
Architecture: any
Depends:
 libm17n-im-config0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: input method configuration library for m17n-lib - utility
 m17n-im-config is a library to create a GTK+ widget for per-user
 configuration of input methods provided by the m17n library.  m17n is
 an abbreviation of Multilingualization.
 .
 This package contains a standalone GUI program using the
 m17n-im-config library.
